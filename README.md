# LCD16x2 v2 #

Danny Yang

### What is this repository for? ###

* Summary of the repository
LCD 16x2 v2 is use to display any input or output we want to display within our Rescuebot.
In our project we used the LCD 16x2 v2 to display when it is "too dark". 
We also used photoresistor as a light sensor which will be discussed in a different repository. 


### How do I get set up? ###

To set the LCD 16x2 v2 I used the information from the tutorial Behind the sceces: CSS-only Clock (Johan van Tongeren).
The library you will need to include is <LiquidCrystal.h>. 
To find this library you go to Sketch > Include Library > Manage Libraries....
You then search LiquidCrystal.h and install that library.
I set pin1 of the LCD to ground. Pin2 to 5 volts. Pin3 to the data out of my 10k potentiometer with a 5 volts supply. 
Pin4 is connected to pin12 of my Max32. Pin5 is to ground. Pin 6 of LCD is to pin11 of Max32.
Pin 11 is connected to pin5. Pin 12 is connected to pin4. Pin 13 is connected to pin3. 
Pin14 is cnnected to pin2. Pin 15 is to 5 volts. Pin 16 is to ground. 

Pins 4, 6, 11, 12, 13, and 14 of your LCD will depend on your code. 
In my code I used these pins accordingly to my function called from the library.  
LiquidCrystal lcd(12, 11, 5, 4, 3, 2);

 

### Contribution guidelines ###


To test my Lcd I used a simple "Hello World!" code that utilize the lcd.print() command.   


The setup of the code include the LiquidCrystal.h library, the function that declares the pins that will power the LCD.
lcd.begin(16,2); this simply set up the LCD's number of columns and rows.
lcd.print("Hello World!"); prints out Hello World! onto the LCD screen. 


* Other guidelines

Johan van Tongeren. Behind the scenes: CSS-only Clock. Retrieved December 06, 2017, from http://www.dreamdealer.nl/tutorials/connecting_a_1602a_lcd_display_and_a_light_sensor_to_arduino_uno.html

### Who do I talk to? ###

Danny Yang